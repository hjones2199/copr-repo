Name:           crosstool-ng
Version:        1.25.0
Release:        1%{?dist}
Summary:        A versatile (cross) toolchain generator
BuildArch:      x86_64

Group: Development/Tools
License: GPLv2+
URL: http://crosstool-ng.org
Source0: https://github.com/crosstool-ng/crosstool-ng/releases/download/crosstool-ng-%{version}/crosstool-ng-%{version}.tar.xz
BuildRequires: ncurses-devel, bison, flex, texinfo, automake, libtool, cvs, lzma, gperf, help2man, wget
Requires: /usr/bin/gmake, glibc-static, libstdc++-static, bison, flex, texinfo, automake, libtool, cvs, lzma, gperf, help2man, wget, autoconf

%description
Crosstool Next Generation is all-in-one solution for generating
tool-chains for cross-compilation.

The tool allows a developer to select and build specific versions
of the tools (gcc, g++, glibc, etc.)

Samples of working configurations are included and can be located
in the %{_libdir}/%{name}-%{version}/samples directory.


%prep
%setup -q -n %{name}-%{version}


%build
./bootstrap
./configure --prefix=%{_prefix} --libdir=%{_libdir}
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .gitignore -print0 | xargs -0 -r rm

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_bindir}/ct-ng
%{_datadir}/%{name}
%{_datadir}/bash-completion/completions/ct-ng
%{_libexecdir}/%{name}
%{_docdir}/%{name}
%{_mandir}/man1/ct-ng.1.gz


%changelog
* Sun Jun 18 2023 Hunter Jones <hjones2199@gmail.com> - 1.25.0
- Update to 1.25.0 release version
* Sat Apr 30 2022 Hunter Jones <hjones2199@gmail.com> - 1.25.0_rc2
- Update to 1.25.0 release candidate
- First version being packaged
* Sun Jan  2 2022 Hunter Jones <hjones2199@gmail.com> - 1.24.0.1
- First version being packaged
